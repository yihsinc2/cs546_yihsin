import os
import sys
import time
import numpy as np
import tensorflow as tf
from tensorflow.contrib.tensorboard.plugins import projector
import rouge_not_a_wrapper as my_rouge
import pdb
import math

FLAGS = tf.app.flags.FLAGS

class Extractor(object):

    def __init__(self, hps, vocab):
        self._hps = hps
        self._vocab = vocab
        if hps.mode == 'train':
            if hps.model == 'end2end' and hps.extractor_loss_in_end2end == False:
                self._graph_mode = 'not_compute_loss'
            else:
                self._graph_mode = 'compute_loss'
        elif hps.mode == 'eval':
            if hps.model == 'end2end':
                if hps.extractor_loss_in_end2end == False or hps.eval_method == 'rouge':
                    self._graph_mode = 'not_compute_loss'
                else:
                    self._graph_mode = 'compute_loss'
            elif hps.model == 'extactor':
                self._graph_mode = 'compute_loss'
        elif hps.mode == 'evalall':
            if hps.decode_method == 'loss':
                self._graph_mode = 'compute_loss'
            else:
                self._graph_mode = 'not_compute_loss'

    def _add_placeholders(self):
        hps = self._hps
        self._art_batch = tf.placeholder(tf.int32, [hps.batch_size, hps.max_art_len, hps.max_sent_len], name="art_batch")
        self._art_lens = tf.placeholder(tf.int32, [hps.batch_size], name="art_lens")
        self._sent_lens = tf.placeholder(tf.int32, [hps.batch_size, hps.max_art_len], name="sent_lens")
        self._art_padding_mask = tf.placeholder(tf.float32, [hps.batch_size, hps.max_art_len], name="art_padding_mask")
        self._sent_padding_mask = tf.placeholder(tf.float32, [hps.batch_size, hps.max_art_len, hps.max_sent_len], name="sent_padding_mask")
        if self._graph_mode == 'compute_loss':
            self._target_batch = tf.placeholder(tf.float32, [hps.batch_size, hps.max_art_len], name="target_batch")

    def _make_feed_dict(self, batch):

        feed_dict = {}
        feed_dict[self._art_batch] = batch.art_batch
        feed_dict[self._art_lens] = batch.art_lens
        feed_dict[self._sent_lens] = batch.sent_lens
        feed_dict[self._art_padding_mask] = batch.art_padding_mask
        feed_dict[self._sent_padding_mask] = batch.sent_padding_mask
        if self._graph_mode == 'compute_loss':
            feed_dict[self._target_batch] = batch.target_batch_extractor

        return feed_dict

    def _add_encoder(self, encoder_inputs, seq_len):

        with tf.variable_scope('encoder'):
            cell_fw = tf.contrib.rnn.GRUCell(self._hps.hidden_dim_extractor)
            cell_bw = tf.contrib.rnn.GRUCell(self._hps.hidden_dim_extractor)
            # we don't need the final state of the encoder since there is no decoder
            encoder_outputs, _ = tf.nn.bidirectional_dynamic_rnn(cell_fw,
                                                                 cell_bw,
                                                                 encoder_inputs,
                                                                 dtype=tf.float32,
                                                                 sequence_length=seq_len,
                                                                 swap_memory=True)
            # concatenate the forward and backward hidden states
            encoder_outputs = tf.concat(axis=2, values=encoder_outputs)

        return encoder_outputs

    def _add_classifier(self, sent_feats, art_feats):

        hps = self._hps
        batch_size = hps.batch_size
        hidden_dim = hps.hidden_dim_extractor

        with tf.variable_scope('classifier'):
            w_content = tf.get_variable('w_content', [2*hidden_dim, 1],
                                        dtype=tf.float32, initializer=self.trunc_norm_init)
            w_salience = tf.get_variable('w_salience', [2*hidden_dim, 2*hidden_dim],
                                         dtype=tf.float32, initializer=self.trunc_norm_init)
            w_novelty = tf.get_variable('w_novelty', [2*hidden_dim, 2*hidden_dim],
                                        dtype=tf.float32, initializer=self.trunc_norm_init)
            bias = tf.get_variable('bias', [1], dtype=tf.float32, initializer=tf.zeros_initializer())

            abs_pos_embed = tf.get_variable('abs_pos_embed', [hps.max_art_len, hps.pos_embed_dim],
                                                 dtype=tf.float32, initializer=self.trunc_norm_init)
            w_abs_pos = tf.get_variable('w_abs_pos', [hps.pos_embed_dim, 1],
                                        dtype=tf.float32, initializer=self.trunc_norm_init)

            rel_pos_embed = tf.get_variable('rel_pos_embed', [hps.num_seg, hps.pos_embed_dim],
                                                 dtype=tf.float32, initializer=self.trunc_norm_init)
            w_rel_pos = tf.get_variable('w_rel_pos', [hps.pos_embed_dim, 1],
                                        dtype=tf.float32, initializer=self.trunc_norm_init)

            s = tf.zeros([batch_size, 2*hidden_dim])
            logits = []
            probs = []

            for i in range(hps.max_art_len):
                content = tf.matmul(sent_feats[:,i,:], w_content) # shape (batch_size, 1)
                salience = tf.reduce_sum(tf.matmul(sent_feats[:,i,:], w_salience) * art_feats, axis=1, keep_dims=True)
                novelty = tf.reduce_sum(tf.matmul(sent_feats[:,i,:], w_novelty) * tf.tanh(s), axis=1, keep_dims=True)
                #abs_index = tf.constant([i], dtype=tf.int32)
                #abs_pos = tf.matmul(tf.nn.embedding_lookup(abs_pos_embed, abs_index), w_abs_pos)
                #rel_index = tf.constant([int(math.floor(i * float(hps.num_seg) / hps.max_art_len))])
                #rel_pos = tf.matmul(tf.nn.embedding_lookup(rel_pos_embed, rel_index), w_rel_pos)
                logit = content + salience - novelty + bias #`+ abs_pos + rel_pos + bias
                logits.append(logit)

                prob = tf.sigmoid(logit)
                probs.append(prob)
                s += tf.multiply(sent_feats[:,i,:], prob)

            return tf.concat(logits, 1), tf.concat(probs, 1)

    def _add_extractor(self):
        hps = self._hps
        vsize = self._vocab.size()  # size of the vocabulary

        with tf.variable_scope('Extractor'):
            self.rand_unif_init = \
                tf.random_uniform_initializer(-hps.rand_unif_init_mag, hps.rand_unif_init_mag, seed=123)
            self.trunc_norm_init = tf.truncated_normal_initializer(stddev=hps.trunc_norm_init_std)

            with tf.variable_scope('embedding'):
                self.embedding = tf.get_variable('embedding', [vsize, hps.emb_dim],
                                                 dtype=tf.float32, initializer=self.trunc_norm_init)
                # got a tensor with shape (batch_size, max_art_len, max_sent_len, emb_dim)
                emb_batch = tf.nn.embedding_lookup(self.embedding, self._art_batch)

            # add word-level encoder
            word_level_enc_inputs = tf.reshape(emb_batch, [-1, hps.max_sent_len, hps.emb_dim])
            sent_lens = tf.reshape(self._sent_lens, [-1])
            with tf.variable_scope('word_level'):
                # with shape (batch_size * max_art_len, max_sent_len, 2*hidden_dim_extractor)
                word_level_enc_outputs = self._add_encoder(word_level_enc_inputs, sent_lens)


            ##########################################################################
            ## merge the word-level representation to sentence-level representation ##
            ##########################################################################
            sent_padding_mask = tf.reshape(self._sent_padding_mask, [-1, hps.max_sent_len, 1])
            sent_lens = tf.reduce_sum(sent_padding_mask, axis=1)
            # since some sentences are padded, which will have zero length
            # directly dividing by sent_lens will result in sigularity
            sent_lens_nonzero = tf.where(sent_lens > 0.0, sent_lens, tf.ones(sent_lens.get_shape().as_list()))
            masked_word_level_enc_outputs = sent_padding_mask * word_level_enc_outputs
            sent_level_enc_inputs = tf.reduce_sum(masked_word_level_enc_outputs, axis=1) / sent_lens_nonzero
            sent_level_enc_inputs = tf.reshape(sent_level_enc_inputs, [-1, hps.max_art_len, 2*hps.hidden_dim_extractor])

            # add sentence-level encoder
            with tf.variable_scope('sent_level'):
                # with shape (batch_size, max_art_len, 2*hidden_dim_extractor)
                sent_level_enc_outputs = self._add_encoder(sent_level_enc_inputs, self._art_lens)

            ##########################################################################################
            ## get the sentence-level representation, it is pretty vague here in the original paper ##
            ##########################################################################################

            sent_feats = tf.contrib.layers.fully_connected(sent_level_enc_outputs,
                                                           2*hps.hidden_dim_extractor,
                                                           activation_fn=tf.tanh)

            ##########################################
            ## get the article-level representation ##
            ##########################################

            art_padding_mask = tf.expand_dims(self._art_padding_mask, 2)
            # with shape (batch_size, 2*hidden_dim_extractor)
            art_feats = tf.reduce_sum(art_padding_mask * sent_level_enc_outputs, axis=1) / \
                        tf.reduce_sum(art_padding_mask, axis=1)
            art_feats = tf.contrib.layers.fully_connected(art_feats, 2*hps.hidden_dim_extractor, activation_fn=tf.tanh)

            logits, probs = self._add_classifier(sent_feats, art_feats)

            self.probs = probs * self._art_padding_mask

            ######################
            ## Compute the loss ##
            ######################
            if self._graph_mode == 'compute_loss':

                with tf.variable_scope('loss'):
                    losses = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits,
                                                                     labels=self._target_batch)  # (batch_size, max_art_len)
                    loss = tf.reduce_sum(losses * self._art_padding_mask, 1) / tf.reduce_sum(self._art_padding_mask,
                                                                                             1)  # (batch_size,)
                    self._loss = tf.reduce_mean(loss)
                    tf.summary.scalar('loss', self._loss)

    def _add_train_op(self):
        """Sets self._train_op, the op to run for training."""
        # Take gradients of the trainable variables w.r.t. the loss function to minimize
        hps = self._hps
        tvars = tf.trainable_variables()
        loss_to_minimize = self._loss
        gradients = tf.gradients(loss_to_minimize, tvars, aggregation_method=tf.AggregationMethod.EXPERIMENTAL_TREE)

        # Clip the gradients
        with tf.device("/gpu:0"):
            grads, global_norm = tf.clip_by_global_norm(gradients, hps.max_grad_norm)

        # Add a summary
        tf.summary.scalar('global_norm', global_norm)

        # Apply adagrad optimizer
        optimizer = tf.train.AdagradOptimizer(hps.lr, initial_accumulator_value=hps.adagrad_init_acc)
        with tf.device("/gpu:0"):
            self._train_op = optimizer.apply_gradients(zip(grads, tvars), global_step=self.global_step,
                                                       name='train_step')

    def build_graph(self):
        """Add the placeholders, model, global step, train_op and summaries to the graph"""
        tf.logging.info('Building graph...')
        tic = time.time()

        self._add_placeholders()
        with tf.device("/gpu:0"):
            self._add_extractor()
        self.global_step = tf.Variable(0, name='global_step', trainable=False)
        if self._hps.mode == 'train':
            self._add_train_op()
        self._summaries = tf.summary.merge_all()
        toc = time.time()
        tf.logging.info('Time to build graph: %i seconds', toc-tic)

    def run_train_step(self, sess, batch):
        """This function will only be called when hps.model == extractor
           Runs one training iteration. Returns a dictionary containing train op,
           summaries, loss, global_step and (optionally) coverage loss."""

        feed_dict = self._make_feed_dict(batch)


        to_return = {
            'train_op': self._train_op,
            'summaries': self._summaries,
            'loss': self._loss,
            'probs': self.probs,
            'global_step': self.global_step,
        }


        return sess.run(to_return, feed_dict)

    def run_eval_step(self, sess, batch):
        """This function will be called when hps.model == extractor or end2end
           Runs one evaluation iteration. Returns a dictionary containing summaries,
           loss, global_step and (optionally) coverage loss."""

        feed_dict = self._make_feed_dict(batch)

        to_return = {
            #'summaries': self._summaries,
            #'loss': self._loss,
            'probs': self.probs,
            #'global_step': self.global_step,
        }

        return sess.run(to_return, feed_dict)
