TRAIN_PATH='/data/VSLab/cindy/Workspace/summarization/data/CNN_Dailymail/finished_files_extract_method3/chunked/train_*'
VAL_PATH='/data/VSLab/cindy/Workspace/summarization/data/CNN_Dailymail/finished_files_extract_method3/chunked/val_*'
TEST_PATH='/data/VSLab/cindy/Workspace/summarization/data/CNN_Dailymail/finished_files_extract_method3/chunked/test_*'
VOCAB_PATH='/data/VSLab/cindy/Workspace/summarization/data/CNN_Dailymail/finished_files_extract_method3/vocab'
EXP_NAME='end2end'
MAX_ITER=50000
BATCH_SIZE=8
MAX_ART_LEN=50
MAX_ENC_STEPS=600
LR=0.01
ADD_EXTRACTOR_LOSS=True
EXTRACTOR_LOSS_WT=5.0
INCONSISTENT_LOSS=True
INCONSISTENT_TOPK=3
SAVE_MODEL_EVERY=1000
MAX_TO_KEEP=30
EXTRACTOR_PATH='log/extractor/pretrain_extractor/train/model.ckpt-27350'
ABSTRACTOR_PATH='log/abstractor/pretrain_abstractor/train/model.ckpt_cov-85000'
END2END_PATH='log/end2end/end2end/train/model.ckpt_cov-31000'

# for eval mode
EVAL_METHOD='rouge'
DECODE_METHOD='greedy'
START_EVAL=1000

# for evalall mode
CKPT_PATH="log/end2end/$EXP_NAME/eval_val_rouge/bestmodel-31000"


#################
#MODE='train'
#MODE='eval'
MODE='evalall'
#################


if [ "$MODE" = "train" ]
then
  #python main.py --model=end2end --mode=train --data_path=$TRAIN_PATH --vocab_path=$VOCAB_PATH --log_root=log --exp_name=$EXP_NAME --max_enc_steps=$MAX_ENC_STEPS --max_dec_steps=100 --max_train_iter=$MAX_ITER --batch_size=$BATCH_SIZE --max_art_len=$MAX_ART_LEN --lr=$LR --extractor_loss_in_end2end=$ADD_EXTRACTOR_LOSS --extractor_loss_wt=$EXTRACTOR_LOSS_WT --inconsistent_loss=$INCONSISTENT_LOSS --inconsistent_topk=$INCONSISTENT_TOPK --save_model_every=$SAVE_MODEL_EVERY --model_max_to_keep=$MAX_TO_KEEP --coverage=True --pretrained_extractor_path=$EXTRACTOR_PATH --pretrained_abstractor_path=$ABSTRACTOR_PATH
  # continue  training from previous step
  python main.py --model=end2end --mode=train --data_path=$TRAIN_PATH --vocab_path=$VOCAB_PATH --log_root=log --exp_name=$EXP_NAME --max_enc_steps=$MAX_ENC_STEPS --max_dec_steps=100 --max_train_iter=$MAX_ITER --batch_size=$BATCH_SIZE --max_art_len=$MAX_ART_LEN --lr=$LR --extractor_loss_in_end2end=$ADD_EXTRACTOR_LOSS --extractor_loss_wt=$EXTRACTOR_LOSS_WT --inconsistent_loss=$INCONSISTENT_LOSS --inconsistent_topk=$INCONSISTENT_TOPK --save_model_every=$SAVE_MODEL_EVERY --model_max_to_keep=$MAX_TO_KEEP --coverage=True --pretrained_end2end_path=$END2END_PATH
elif [ "$MODE" = "eval" ]
then
  python main.py --model=end2end --mode=eval --data_path=$VAL_PATH --vocab_path=$VOCAB_PATH --log_root=log --exp_name=$EXP_NAME --max_enc_steps=$MAX_ENC_STEPS --max_dec_steps=120 --max_art_len=$MAX_ART_LEN --batch_size=64 --extractor_loss_in_end2end=$ADD_EXTRACTOR_LOSS --extractor_loss_wt=$EXTRACTOR_LOSS_WT --inconsistent_loss=$INCONSISTENT_LOSS --inconsistent_topk=$INCONSISTENT_TOPK --eval_method=$EVAL_METHOD --decode_method=$DECODE_METHOD --start_eval_rouge=$START_EVAL --save_model_every=$SAVE_MODEL_EVERY --single_pass=1 --coverage=True
elif [ "$MODE" = "evalall" ]
then
  python main.py --model=end2end --mode=evalall --data_path=$TEST_PATH --vocab_path=$VOCAB_PATH --log_root=log --exp_name=$EXP_NAME --max_enc_steps=$MAX_ENC_STEPS --max_dec_steps=120 --max_art_len=$MAX_ART_LEN --decode_method=beam --coverage=True --single_pass=1 --save_pkl=True --save_vis=True --extractor_loss_in_end2end=$ADD_EXTRACTOR_LOSS --inconsistent_loss=True --inconsistent_topk=3 --eval_ckpt_path=$CKPT_PATH
fi
